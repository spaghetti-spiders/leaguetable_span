import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
// For quickly checking if the team name already exists we use HashMaps. This will take our code from O(n) to O(log n) complexity where N is the amount score of entries.
import java.util.HashMap;

public class CaptureScores {
    static final int scoreForWinning = 3;
    static final int scoreForDrawing = 1;
    static final int scoreForLosing = 0;

    public static void main(String[] args) {
        HashMap<String, Integer> teamsScoreMap = captureTeamScoresToHashMap(args);
        ArrayList<Team> teamsArrayList = convertHashMapToTeamArrayList(teamsScoreMap);
        teamsArrayList.sort(new TeamComparator());
        printLeagueResults(teamsArrayList);
    }

    public static HashMap<String, Integer> captureTeamScoresToHashMap(String[] args) {
        if (args.length != 0) {
            return readFromFile(args);
        }
        return readFromCommandLine();
    }

    public static HashMap<String, Integer> readFromFile(String[] args) {
        HashMap<String, Integer> teamsScoreMap = new HashMap<>();
        System.out.print("Thank you for uploading a file \n");
        try {
            File leagueScores = new File(args[0]);
            Scanner fileReader = new Scanner(leagueScores);
            while (fileReader.hasNextLine()) {
                teamsScoreMap = populateHashMapFromData(teamsScoreMap, fileReader.nextLine());
            }
            fileReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Your file could not be found. Please confirm your file name and location");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Something went wrong. Please check your data format");
            e.printStackTrace();
        }
        return teamsScoreMap;
    }

    public static HashMap<String, Integer> readFromCommandLine() {
        HashMap<String, Integer> teamsScoreMap = new HashMap<>();
        System.out.print("Welcome to the Score Uploader 9000. Please enter your scores and type \"Stop\" to stop. \n");
        Scanner scanner = new Scanner(System.in);
        String scannerLine;
        try {
            //Instead of using CTRL+D. We can check the nextLine's value here and stop if it is empty. hasNextLine() will always be true due to using System.in
            while (scanner.hasNextLine() && !(scannerLine = scanner.nextLine()).equals("Stop")) {
                teamsScoreMap = populateHashMapFromData(teamsScoreMap, scannerLine);
            }
            scanner.close();
        } catch (Exception e) {
            System.out.print("Something went wrong. Please check the format of your input");
            e.printStackTrace();
        }
        return teamsScoreMap;
    }

    public static HashMap<String, Integer> populateHashMapFromData(HashMap<String, Integer> hashMap, String nextLine) {
        String[] capturedValuesArray = nextLine.split(",\\s|\\s(?=\\d)");
        if (capturedValuesArray.length == 4) { // This length check is to sanitize data
            String firstTeamName = capturedValuesArray[0];
            int firstTeamScore = Integer.parseInt(capturedValuesArray[1]);
            int firstTeamLeaguePoints;

            String secondTeamName = capturedValuesArray[2];
            int secondTeamScore = Integer.parseInt(capturedValuesArray[3]);
            int secondTeamLeaguePointsEarned;

            if (firstTeamScore > secondTeamScore) {
                firstTeamLeaguePoints = scoreForWinning;
                secondTeamLeaguePointsEarned = scoreForLosing;
            } else if (firstTeamScore < secondTeamScore) {
                firstTeamLeaguePoints = scoreForLosing;
                secondTeamLeaguePointsEarned = scoreForWinning;
            } else {
                firstTeamLeaguePoints = scoreForDrawing;
                secondTeamLeaguePointsEarned = scoreForDrawing;
            }

            if (hashMap.containsKey(firstTeamName)) {
                hashMap.replace(firstTeamName, hashMap.get(firstTeamName) + firstTeamLeaguePoints);
            } else {
                hashMap.put(firstTeamName, firstTeamLeaguePoints);
            }
            if (hashMap.containsKey(secondTeamName)) {
                hashMap.replace(secondTeamName, hashMap.get(secondTeamName) + secondTeamLeaguePointsEarned);
            } else {
                hashMap.put(secondTeamName, secondTeamLeaguePointsEarned);
            }
        }
        return hashMap;
    }

    public static ArrayList<Team> convertHashMapToTeamArrayList(HashMap<String, Integer> map) {
        ArrayList<Team> teamsArrayList = new ArrayList<>();
        for (HashMap.Entry<String, Integer> mapEntry : map.entrySet()) {
            teamsArrayList.add(new Team(mapEntry.getKey(), mapEntry.getValue()));
        }
        return teamsArrayList;
    }

    public static ArrayList<String> printLeagueResults(ArrayList<Team> teamResultsArrayList) {
        ArrayList<String> teamResultDisplayArrayList = new ArrayList<>();
        int previousPosition = 0;
        for (int i = 0; i < teamResultsArrayList.size(); i++) {
            String pointsPostfix = teamResultsArrayList.get(i).getTeamLeagueScore() == 1 ? "pt" : "pts";
            int position = determinePosition(teamResultsArrayList, i, previousPosition);
            String output = position + 1 + ". " + teamResultsArrayList.get(i).teamName + ", " + teamResultsArrayList.get(i).getTeamLeagueScore() + " " + pointsPostfix + "\n";
            previousPosition = position;

            teamResultDisplayArrayList.add(output);
            System.out.print(output);
        }
        return teamResultDisplayArrayList;
    }

    public static int determinePosition(ArrayList<Team> teamResultsArrayList, int index, int previousPosition) {
        if (index == 0) {
            return index;
        }
        if (teamResultsArrayList.get(index).getTeamLeagueScore() == teamResultsArrayList.get(index - 1).getTeamLeagueScore()) {
            return previousPosition;
        }
        return index;
    }
}
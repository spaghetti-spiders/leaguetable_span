import java.util.Comparator;

public class TeamComparator implements Comparator<Team> {
    public int compare(Team a, Team b) {
        int pointComparison = Integer.compare(b.getTeamLeagueScore(),a.getTeamLeagueScore());
        return pointComparison == 0 ? a.getTeamName().compareTo(b.getTeamName()) : pointComparison;
    }
}
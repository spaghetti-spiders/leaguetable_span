/* I Planned on implementing Lombok here, after spending too much time trying to get the idea to recognize it I stopped.
    I Looked at implementing a record structure for this object, but decided against it as Java 14+ might not be familiar to everyone.
 */
public class Team {
    public final String teamName;
    public int teamLeagueScore;

    public String getTeamName() {
        return teamName;
    }

    public int getTeamLeagueScore() {
        return teamLeagueScore;
    }

    public Team(String tName, int tLeagueScore){
        teamName = tName;
        teamLeagueScore = tLeagueScore;
    }

    @Override
    public String toString(){
        return teamName + teamLeagueScore;
    }
}
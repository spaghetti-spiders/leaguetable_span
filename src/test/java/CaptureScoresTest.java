import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.HashMap;

public class CaptureScoresTest {

    @Test
    public void testMapStringsToHashMap() {
        String expectedInput1 = "Lions 3, Snakes 3";
        String expectedInput2 = "Tarantulas 1, FC Awesome 0";
        String expectedInput3 = "Lions 1, FC Awesome 1";
        String expectedInput4 = "Tarantulas 3, Snakes 1";
        String expectedInput5 = "Lions 4, Grouches 0";

        HashMap<String, Integer> expectedOutput = new HashMap<>();
        expectedOutput.put("Lions", 5);
        expectedOutput.put("Snakes", 1);
        expectedOutput.put("Tarantulas", 6);
        expectedOutput.put("FC Awesome", 1);
        expectedOutput.put("Grouches", 0);

        HashMap<String, Integer> map = new HashMap<>();
        map = CaptureScores.populateHashMapFromData(map, expectedInput1);
        map = CaptureScores.populateHashMapFromData(map, expectedInput2);
        map = CaptureScores.populateHashMapFromData(map, expectedInput3);
        map = CaptureScores.populateHashMapFromData(map, expectedInput4);
        map = CaptureScores.populateHashMapFromData(map, expectedInput5);

        Assertions.assertEquals(map, expectedOutput);
    }

    @Test
    public void testConvertHashMapToOrderedTeamArrayList() {
        HashMap<String, Integer> expectedInput = new HashMap<>();
        expectedInput.put("Lions", 5);
        expectedInput.put("Snakes", 1);
        expectedInput.put("Tarantulas", 6);
        expectedInput.put("FC Awesome", 1);
        expectedInput.put("Grouches", 0);

        ArrayList<Team> expectedOutput = new ArrayList<>();
        expectedOutput.add(new Team("Tarantulas", 6));
        expectedOutput.add(new Team("Lions", 5));
        expectedOutput.add(new Team("FC Awesome", 1));
        expectedOutput.add(new Team("Snakes", 1));
        expectedOutput.add(new Team("Grouches", 0));

        ArrayList<Team> arrayList;
        arrayList = CaptureScores.convertHashMapToTeamArrayList(expectedInput);
        arrayList.sort(new TeamComparator());

        Assertions.assertEquals(arrayList.get(0).toString(), expectedOutput.get(0).toString());
        Assertions.assertEquals(arrayList.get(1).toString(), expectedOutput.get(1).toString());
        Assertions.assertEquals(arrayList.get(2).toString(), expectedOutput.get(2).toString());
        Assertions.assertEquals(arrayList.get(3).toString(), expectedOutput.get(3).toString());
        Assertions.assertEquals(arrayList.get(4).toString(), expectedOutput.get(4).toString());
    }

    @Test
    public void testPrintLeagueResults() {
        ArrayList<Team> expectedInput = new ArrayList<>();
        expectedInput.add(new Team("Tarantulas", 6));
        expectedInput.add(new Team("Lions", 5));
        expectedInput.add(new Team("FC Awesome", 1));
        expectedInput.add(new Team("Snakes", 1));
        expectedInput.add(new Team("Grouches", 0));

        ArrayList<String> expectedOutput = new ArrayList<>();
        expectedOutput.add("1. Tarantulas, 6 pts\n");
        expectedOutput.add("2. Lions, 5 pts\n");
        expectedOutput.add("3. FC Awesome, 1 pt\n");
        expectedOutput.add("3. Snakes, 1 pt\n");
        expectedOutput.add("5. Grouches, 0 pts\n");

        ArrayList<String> arrayList;
        arrayList = CaptureScores.printLeagueResults(expectedInput);

        Assertions.assertEquals(arrayList.get(0).toString(), expectedOutput.get(0).toString());
        Assertions.assertEquals(arrayList.get(1).toString(), expectedOutput.get(1).toString());
        Assertions.assertEquals(arrayList.get(2).toString(), expectedOutput.get(2).toString());
        Assertions.assertEquals(arrayList.get(3).toString(), expectedOutput.get(3).toString());
        Assertions.assertEquals(arrayList.get(4).toString(), expectedOutput.get(4).toString());

    }
}
League Table Generator 9001

This program allows you to enter the results of league games and receive an ordered ranking of the results.

You can pass the location of your data file or enter the data manually after executing the code.